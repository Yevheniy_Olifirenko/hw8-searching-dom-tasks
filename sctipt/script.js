// Task 1

const eachP = document.querySelectorAll("p");
    for (let elP of eachP) {
        elP.style.backgroundColor = "#ff0000";
    }

// Task 2

let findElem = document.getElementById("optionsList");

console.log("Firts element id='optionsList': ", findElem);
console.log("🡡 Parent element: ", findElem.parentElement);

findElem.hasChildNodes();
console.log("🡣 Child elements: ", findElem.childNodes);

    for (let elNode of findElem.childNodes) {
        console.log("Node name: ", elNode.nodeName);
        console.log("Node type: ", elNode.nodeType);
    }

// Task 3

const addText = document.getElementById("testParagraph");
addText.textContent = "This is a paragraph";

// Task 4

const mainHeader = document.querySelector(".main-header");
console.log("Children of 'main-header': ", mainHeader.children);

mainHeader.classList.add("nav-item");
console.log("New class of 'main-header': ", mainHeader);

// Task 5

let removeClass = document.querySelectorAll(".section-title");
console.log("class = 'section-title': ", removeClass);

    for (let elemCl of removeClass) {
        elemCl.classList.remove("section-title");
    }
console.log("class = 'section-title' deleted: ", removeClass);
